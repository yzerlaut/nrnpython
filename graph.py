#!/usr/bin/env python
#coding=utf-8

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, collections, transforms, colors, ticker
import field

def plot_neuron(coords, scalar=None, colors=None,
                norm=colors.Normalize(), cmap=cm.jet,
                polar_angle=0, azimuth_angle=np.pi/2., center = [0,0,0]):
    """
    takes the segment coordinates and plot the neuron
    ------------------------------------------------------------------------------
    the view of the 2D projection of the 3D neuron can be set with
    two angles [azimuth, polar] from a center point [x0, y0, z0]
    Conventions for the angles are standard spherical coordinates :
    - polar = 0 means view perpendicularly to the x-axis  (default)
    - azimuth=pi/2 means view perpendicularly to the z-axis (default)
    ------------------------------------------------------------------------------
    the color of each segment can be adjusted with a scalar value
    or with a color ???
    """
   
    a = plt.gca()
    [x0, y0, z0] = center
    line_segs = [[\
                 ( np.cos(polar_angle)*(c['x0']-x0)+np.sin(polar_angle)*(c['y0']-y0)
                                , (c['z0']-z0) ),
                 ( np.cos(polar_angle)*(c['x1']-x0)+np.sin(polar_angle)*(c['y1']-y0)
                                , (c['z1']-z0) )\
                  ] for c in coords]

    col = collections.LineCollection(line_segs,\
                                cmap=cmap, norm=norm)
    a.add_collection(col, autolim=True)
    if scalar is not None:
        col.set_array(scalar)
    else:
        col.set_color(colors)

    a.autoscale_view()
    plt.axis('equal')
    return col

def logcontour(xx, yy, zz, n_contours=10):
    
    v = np.logspace(np.log10(np.min(zz[:])),
                    np.log10(np.max(zz[:])), n_contours)
    lev_exp = np.arange(np.floor(np.log10(v.min())-1),
                           np.floor(np.log10(v.max())+1))
    
    levs = np.power(10, lev_exp)*np.array([1, 2, 5])[:, np.newaxis]
    levs = np.hstack(levs).astype(int)
   
    def pow_fmt(q, m):
        if (m < 2) and (m > 0):
            return "%d" % (10**m * q)
        if q == 1:
            return  r"$10^{%d}$" % (m,)
        else:
            return r"$%d\cdot10^{%d}$" % (q,m)


    fmt = [ pow_fmt(q,m) for q in [1,2,5] for m in lev_exp]

    fmt = dict(zip(levs, fmt))

    cs = plt.contour(xx, yy, zz, levs, norm=colors.LogNorm() )
    plt.clabel(cs, cs.levels, fmt=fmt, inline=1)

def plot_multiplies(xx, yy, vv, t=None, w=0.1, h=0.1, sharey=True):
    """Plot small mutiplies of potential  on a grid
    
    * w, h -- multiplies width/height in axes coords.
    
    Notes:
    You have to make sure that the main axis limits are set correctly
    prior to plotting."""


    def axes_locator(xx, yy):
        def __ax_loc(ax_inset, renderer):
            transDataToFigure = (ax.transData+fig.transFigure.inverted())
            x, y = transDataToFigure.transform((xx, yy))
            bbox = transforms.Bbox.from_bounds(x-w/2., y-h/2., w, h)
            return bbox
        return __ax_loc
        

    if t is None:
        t = np.arange(vv.shape[0])
    fig = plt.gcf() 
    ax = plt.gca()
    
    # calc transform for inset placement
    transDataToFigure = (ax.transData+fig.transFigure.inverted())
    ax_inset = []
    lines = []
    last_inset = None
    nx, ny = xx.shape
    for i in range(nx):
        for j in range(ny):
            x, y = transDataToFigure.transform((xx[i,j], yy[i,j]))
            last_inset = fig.add_axes([x-w/2., y-h/2., w, h], frameon=False,
                              sharey=last_inset if sharey else None, 
                              sharex=last_inset)
            last_inset.set_axes_locator(axes_locator(xx[i,j],yy[i,j]))
            l, = plt.plot(t, vv[:,i,j], 'k-')
            plt.xticks([])
            plt.yticks([])
            ax_inset.append(last_inset)
            lines.append(l)
    return lines, ax_inset
