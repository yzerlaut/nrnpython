from neuron import h as nrn
import numpy as np

# def load_dll(folder='/home/yann/work/nrnpython/'):
#     nrn.load_nrn_dll(folder+'')


def fill_params_of_point_process(point_process, params_dict):
    """
    set the parameters of a point process mechanisms with the use
    of a dictionary
    Each entry of the dict() is set to its value with he point process
    """
    for k, value in params_dict.iteritems():
        exec('point_process.'+k+' = '+str(value))

        
def insert_on_membrane(channel_prop, section):
    """ Insert a mechanism into 'section' and sets its parameters
    the mechanism is passed as the the string 'NAME'   """
    section.insert(channel_prop['NAME'])
    for key, value in channel_prop.iteritems():
        if key<>'NAME':
            exec('section.'+key+'_'+channel_prop['NAME']+' = '+str(value))
            
def set_on_membrane(membrane_prop, section):
    """ set the parameters of a a section's membrane
    typically : cm, Ra, ena, ek, ...
    If the ions are not defined -> problem ! (so exception)
    """
    for key, value in membrane_prop.iteritems():
        try:
            exec('section.'+key+' = '+str(value))
        except NameError:
            # print key, ' not useful in ', section
            do_nothing = True

            
def set_synapses_onto_segment(segment, section, Synapse_Type,\
            synaptic_params, nsyn=1, unit_area_per_synapse=None):
    """
    we lump all the different synapses of a segment onto a unique synapse
    with a presynaptic event generator of type corrgen8.mod, 
    where we can set a number and a correlation coefficient between
    this presynaptic units
    """
    nsyn = int(.5+area/unit_area_per_synapse) # synapses number
    distance = nrn.distance(segment.x, sec=section)
    synapse = Synapse_Type(segment.x, sec=section) # of the type nrn.Mechanism !
    fill_params_of_point_process(synapse, synaptic_params) # filling its properties
    synapse.loc(segment.x, sec=section) # postsynaptic is present compartment
    synapse.allocate(nsyn) # allocate for nsyn synapses
    return synapse, nsyn, distance
